/*
 * Copyright (c) 2008,2009, Emile "iMil" Heitor <imil@gcu.info>
 * All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

/* $Id: slsym.h,v 1.3 2008/12/10 13:28:22 imilh Exp $ */

#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <elf.h>

typedef struct Elf_Handler {
	struct stat	sb;
	void		*map;
	Elf_Sym		*symtbl;
	Elf_Sym		*symtbl_end;
	char		*strtbl;
} Elf_Handler;

Elf_Handler *sl_open(char *);
void *sl_sym(Elf_Handler *, char *);
void sl_close(Elf_Handler *);
