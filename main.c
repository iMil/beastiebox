/*
 * Copyright (c) 2008,2009, Emile "iMil" Heitor <imil@gcu.info>
 * All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

/* $Id: main.c,v 1.14 2008/12/10 13:23:14 imilh Exp $ */

#include "slsym.h"
#include "tools.h"
#include <dlfcn.h>

#define CMDNAME "command"
#define CMDDIR "beastielib"

const char *progname = "beastiebox";
char *init_cmd = "init";
char *test_cmd = "test";
char *reboot_cmd = "reboot";

int main(int argc, char *argv[]) {
	void *hdl = NULL;
	Elf_Handler *eh = NULL;
	int (*sbl)(int, char **);
	int rc = 0, i;
	char *argv0, cmd[MAXLEN];

	argv0 = strrchr(argv[0], '/');
	if (argv0 == NULL)
		argv0 = argv[0];
	else
		argv0++;

	/* exec from kernel and init prepends a dash on argv[0] */
	if (*argv0 == '-')
		argv0++;

	/* argv[0] is not set when init is called */
	if (*argv0 == '\0')
		argv0 = init_cmd;

	/* [ / test special case */
	if (*argv0 == '[')
		argv0 = test_cmd;

	/* reboot / halt / poweroff */
	if (strcmp(argv0, "halt") == 0 || strcmp(argv0, "poweroff") == 0)
		argv0 = reboot_cmd;

	snprintf(cmd, MAXLEN, "%s/lib%s.so", CMDDIR, argv0);

	if ((hdl = dlopen(cmd, RTLD_LAZY)) == NULL)
		/* no matching shlib, let's try our own symbols */
		hdl = RTLD_SELF;

	snprintf(cmd, MAXLEN, "%s_%s", CMDNAME, argv0);

	if ((sbl = dlsym(hdl, cmd)) == NULL) {
		if (hdl != RTLD_SELF)
			dlclose(hdl);

		/* function was not found in a shared object, try static */
		eh = sl_open(argv[0]);
		if ((sbl = sl_sym(eh, cmd)) == NULL) {
			sl_close(eh);
			errx(EXIT_FAILURE, "no such function %s\n", argv0);
		}
	}

	rc = (sbl)(argc, argv);

	if (hdl != RTLD_SELF)
		dlclose(hdl);
	if (eh != NULL)
		sl_close(eh);

	return rc;
}
