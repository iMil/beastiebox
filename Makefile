.include <bsd.own.mk>
.include "${.CURDIR}/cmdlist"
.include "${.CURDIR}/Makefile.inc"

PROG=	beastiebox
SRCS=	main.c tools.c slsym.c
NOMAN=	# defined
MKMAN=	no

MKCRYPT=	${CMDLIST:Mlogin}

# installation directory
.if defined(BBINST)
DESTDIR=	${BBINST}
.endif

.if !defined(ELFSIZE)
ELFSIZE=32
.endif

CFLAGS+=	-DELFSIZE=${ELFSIZE}

# BBBSDSRCDIR comes from Makefile.inc

UTIL=	${BBBSDSRCDIR}/lib/libutil
FSCK=	${BBBSDSRCDIR}/sbin/fsck
KVM=	${BBBSDSRCDIR}/lib/libkvm
TERM=	${BBBSDSRCDIR}/lib/libterm
CRYPT=	${BBBSDSRCDIR}/lib/libcrypt

.PATH:	${UTIL} ${FSCK} ${KVM} ${TERM} ${CRYPT}
# libutil light
SRCS+=	efun.c snprintb.c if_media.c login_tty.c logout.c logwtmp.c		\
	logwtmpx.c opendisk.c getrawpartition.c getlabelsector.c			\
	getmaxpartitions.c getmntopts.c parsedate.y pidlock.c ttyaction.c
# needed by fsck*
SRCS+=	fsutil.c
CFLAGS+=	-I${FSCK}

# needed by ps and dmesg
SRCS+=	kvm.c kvm_proc.c
.if exists(kvm_${MACHINE_ARCH}.c)
SRCS+=	kvm_${MACHINE_ARCH}.c
.elif	exists(kvm_${MACHINE_CPU}.c)
SRCS+=	kvm_${MACHINE_CPU}.c
.else
.BEGIN:
	@echo no kvm_xx.c for ${MACHINE_ARCH} nor ${MACHINE_CPU}
	@false
.endif

# termcap, needed by less, vi and friends
SRCS+=	termcap.c tgoto.c tputs.c tputws.c
CFLAGS+=	-I${TERM}

# either to build libcrypt or not
.if !empty(MKCRYPT)
SRCS+=		login.c login_cap.c secure_path.c # from libutil
SRCS+=		crypt.c md5crypt.c bcrypt.c crypt-sha1.c util.c pw_gensalt.c
SRCS+=		hmac_sha1.c
.endif

CFLAGS+=	-DBBOX #-DDEBUG
# needed for self symbol lookup
LDFLAGS+=	-rdynamic

DPADD+=	${LIBM}
LDADD+=	-lm

.if defined(FULLSTATIC)
LDFLAGS+=	-static
STATIC=		# defined
.endif

.if defined(STATIC)
LDFLAGS+=	-Wl,--whole-archive beastielib/*_pic.a \
		-Wl,--no-whole-archive
.endif

#SUBDIR=	beastiebin

bbclean:
	rm -f ${PROG} ${OBJS}

strip:
	${OBJCOPY} -S -R .note -R .ident -R .comment -R .copyright ${PROG}

staticstrip:
	${OBJCOPY} -S -w -K command_* -R .note -R .ident \
			-R .comment -R .copyright ${PROG}

archive:
	@rm -f beastielib/*.a
	@tar zcvf beastielib.tar.gz beastielib

.include <bsd.prog.mk>
.include <bsd.subdir.mk>
