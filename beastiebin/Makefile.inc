LIBDIR=		${.CURDIR}/../../beastielib
LIBOWN!=	id -un
LIBGRP!=	id -gn
NOPROFILE=	# defined
NOLINT=		# defined
NOMAN=		# defined
MKLINT=		no
MKMAN=		no

CFLAGS+=	-DBBOX

NM?=		nm
OBJCOPY?=	objcopy

MKPROFILE=	no
# the following variable is used in bb.lib.mk
DYNSTRIP=	-S -R .note -R .ident -R .comment -R .copyright

BBBSDSRCDIR=	${.CURDIR}/../../src
