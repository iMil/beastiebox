/*
 * This file was generated by the mkbuiltins program.
 */

#include <sys/cdefs.h>
#define BLTINCMD 0
#define BGCMD 1
#define BREAKCMD 2
#define CDCMD 3
#define DOTCMD 4
#define ECHOCMD 5
#define EVALCMD 6
#define EXECCMD 7
#define EXITCMD 8
#define EXPORTCMD 9
#define FALSECMD 10
#define FGCMD 11
#define GETOPTSCMD 12
#define HASHCMD 13
#define JOBIDCMD 14
#define JOBSCMD 15
#define LCCMD 16
#define LOCALCMD 17
#define PWDCMD 18
#define READCMD 19
#define RETURNCMD 20
#define SETCMD 21
#define SETVARCMD 22
#define SHIFTCMD 23
#define TRAPCMD 24
#define TRUECMD 25
#define UMASKCMD 26
#define UNSETCMD 27
#define WAITCMD 28

struct builtincmd {
      char *name;
      int code;
};

extern int (*const builtinfunc[])();
extern const struct builtincmd builtincmd[];
