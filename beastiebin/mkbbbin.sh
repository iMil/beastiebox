#!/bin/sh

usage()
{
	echo "usage: $0 /path/to/command"
	exit 1
}

[ $# -lt 1 ] && usage

progpath=$1
progname=${progpath##*/}

echo ". copying source"
cp -r $progpath .

cd $progname

echo ". modifying Makefile"
perl -pi.bkp -e 's/^PROG=/LIB=/;s/<bsd.prog.mk>/"..\/bb.lib.mk"/' Makefile

echo ". copying shlib_version"
cp ../echo/shlib_version .

echo ". deleting old CVS"
rm -rf CVS
