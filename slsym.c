/*
 * Copyright (c) 2008,2009, Emile "iMil" Heitor <imil@gcu.info>
 * All rights reserved.
 *
 * See the LICENSE file for redistribution information.
 */

/* $Id: slsym.c,v 1.4 2008/12/11 16:50:05 imilh Exp $ */

#include "slsym.h"

/* ELF static symbol lookup, inspired from ftrace
 *
 * www.cs.cmu.edu/afs/cs.cmu.edu/academic/class/15213-f03/www/ftrace/elf.c
 */

/* These functions works pretty much like DLFCN(3)
 *
 * sl_open() returns an ELF handler
 * sl_sym()  returns a function pointer
 * sl_close() frees the handler and mmaped memory
 *
 */

Elf_Handler *sl_open(char *path) {
	int fd, i;
	struct stat sb;
	Elf_Handler *ehdl = NULL;
	Elf_Ehdr *ehdr;
	Elf_Shdr *shdr;

	if ((fd = open(path, O_RDONLY)) < 0)
		err("open");
	if ((fstat(fd, &sb)) < 0)
		err("fstat");

	if ((ehdl = (Elf_Handler *)malloc(sizeof(Elf_Handler))) == NULL)
		err("malloc");

	ehdl->sb = sb;

	if (ehdl->sb.st_size < sizeof(Elf_Ehdr)) {
		fprintf(stderr, "\"%s\" is not an ELF object\n", path);
		return NULL;
	}

	ehdl->map = mmap(NULL, sb.st_size, PROT_READ, MAP_FILE | MAP_SHARED, fd,
			 (off_t)0);
	if (ehdl->map == MAP_FAILED)
		err("mmap");

	close(fd);

	ehdr = (Elf_Ehdr *)ehdl->map;

	if (strncmp(ehdr->e_ident, ELFMAG, SELFMAG) != 0) {
		fprintf(stderr, "\"%s\" is not an ELF object\n", path);
		return NULL;
	}

	/* point to section headers */
	shdr = (Elf_Shdr *)(ehdl->map + ehdr->e_shoff);

	ehdl->symtbl = NULL;

	for (i = 0; i < ehdr->e_shnum; i++) {
		/* find symbol table */
		if (shdr[i].sh_type == SHT_SYMTAB) {
			ehdl->symtbl =
			    (Elf_Sym *)(ehdl->map + shdr[i].sh_offset);
			ehdl->symtbl_end =
			    (Elf_Sym *)((char *)ehdl->symtbl + shdr[i].sh_size);
			ehdl->strtbl =
			    (char *)(ehdl->map +
				     shdr[shdr[i].sh_link].sh_offset);
		}
	}

	if (ehdl->symtbl == NULL) {
		fprintf(stderr, "symbol table not found !\n");
		return NULL;
	}

	return ehdl;
}

void *sl_sym(Elf_Handler *ehdl, char *symname) {
	Elf_Sym *psymt;

	for (psymt = ehdl->symtbl; psymt < ehdl->symtbl_end; psymt++)
		if ((strcmp(&ehdl->strtbl[psymt->st_name], symname) == 0) &&
		    ELF_ST_TYPE(psymt->st_info) == STT_FUNC)
			return (void *)psymt->st_value;

	return NULL;
}

void sl_close(Elf_Handler *ehdl) {
	if (ehdl == NULL)
		return;

	if (munmap(ehdl->map, ehdl->sb.st_size) < 0)
		err("munmap");

	free(ehdl);
}
